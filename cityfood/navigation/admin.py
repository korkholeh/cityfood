# coding: utf-8
from django.contrib import admin
from .models import NavigationItem
from suit.admin import SortableModelAdmin


class NavigationItemAdmin(SortableModelAdmin):
    list_display = ['name', 'url', 'regex', 'visible']
    search_fields = ['name']
    sortable = 'position'

admin.site.register(NavigationItem, NavigationItemAdmin)
