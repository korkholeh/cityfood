# encoding: utf8
from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('navigation', '0002_auto_20140424_1038'),
    ]

    operations = [
        migrations.AddField(
            model_name='navigationitem',
            name='visible_in_top_bar',
            field=models.BooleanField(default=False, verbose_name=u'Visible in top bar'),
            preserve_default=True,
        ),
    ]
