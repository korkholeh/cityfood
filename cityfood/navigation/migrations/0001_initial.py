# encoding: utf8
from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='NavigationItem',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name=u'Name')),
                ('url', models.URLField(verbose_name=u'URL')),
                ('position', models.IntegerField(default=1, verbose_name=u'Position')),
            ],
            options={
                u'verbose_name': u'Navigation item',
                u'verbose_name_plural': u'Navigation items',
            },
            bases=(models.Model,),
        ),
    ]
