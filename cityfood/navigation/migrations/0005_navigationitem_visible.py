# encoding: utf8
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        (b'navigation', b'0004_auto_20140512_1125'),
    ]

    operations = [
        migrations.AddField(
            model_name=b'navigationitem',
            name=b'visible',
            field=models.BooleanField(default=True, verbose_name='\u0412\u0438\u0434\u0438\u043c\u044b\u0439'),
            preserve_default=True,
        ),
    ]
