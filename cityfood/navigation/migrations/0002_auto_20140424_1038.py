# encoding: utf8
from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('navigation', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='navigationitem',
            name='regex',
            field=models.CharField(default='r', max_length=200, verbose_name=u'Regex'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='navigationitem',
            name='url',
            field=models.CharField(max_length=200, verbose_name=u'URL'),
        ),
    ]
