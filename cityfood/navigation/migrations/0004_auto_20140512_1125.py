# encoding: utf8
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        (b'navigation', b'0003_navigationitem_visible_in_top_bar'),
    ]

    operations = [
        migrations.RemoveField(
            model_name=b'navigationitem',
            name=b'visible_in_top_bar',
        ),
        migrations.AlterField(
            model_name=b'navigationitem',
            name=b'name',
            field=models.CharField(max_length=100, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435'),
        ),
        migrations.AlterField(
            model_name=b'navigationitem',
            name=b'position',
            field=models.IntegerField(default=1, verbose_name='\u041f\u043e\u0437\u0438\u0446\u0438\u044f'),
        ),
        migrations.AlterField(
            model_name=b'navigationitem',
            name=b'url',
            field=models.CharField(max_length=200, verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430'),
        ),
        migrations.AlterField(
            model_name=b'navigationitem',
            name=b'regex',
            field=models.CharField(max_length=200, verbose_name='\u0420\u0435\u0433\u0443\u043b\u044f\u0440\u043d\u043e\u0435 \u0432\u044b\u0440\u0430\u0436\u0435\u043d\u0438\u0435'),
        ),
    ]
