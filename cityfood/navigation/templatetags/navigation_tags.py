# coding: utf-8
import re
from django import template
from navigation.models import NavigationItem


register = template.Library()


@register.inclusion_tag(
    'navigation/nav_bar.html', takes_context=True)
def nav_bar(context):
    request = context['request']
    items = NavigationItem.objects.all()
    for item in items:
        pattern = re.compile(item.regex)
        m = re.match(pattern, request.get_full_path())
        if m:
            item.active = True
        else:
            item.active = False

    return {
        'items': items,
    }

