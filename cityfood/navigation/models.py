# coding: utf-8
from django.db import models


class NavigationItem(models.Model):
    name = models.CharField(u'Название', max_length=100)
    visible = models.BooleanField(u'Видимый', default=True)
    url = models.CharField(u'Ссылка', max_length=200)
    regex = models.CharField(u'Регулярное выражение', max_length=200)
    position = models.IntegerField(u'Позиция', default=1)

    class Meta:
        verbose_name = u'Элемент навигации'
        verbose_name_plural = u'Элементы навигации'
        ordering = ['position']

    def __unicode__(self):
        return self.name
