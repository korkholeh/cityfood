# coding: utf-8
from django.conf.urls import patterns, url


urlpatterns = patterns(
    'order.views',
    url(r'success/$', 'order_success', name='order_success'),
    url(r'$', 'order', name='order'),
)
