# coding: utf-8
from django.http import HttpResponse, Http404
from django.shortcuts import render_to_response, redirect
from django.core.urlresolvers import reverse
from django.template import RequestContext
from .forms import OrderForm
from .models import Order, OrderItem, OrderStatus
from django.core.mail import send_mail
from django.conf import settings


def order(request):
    from catalog.models import Product
    products = Product.objects.filter(active=True)

    non_active = Product.objects.filter(active=False)
    products = products.exclude(linked_products__in=non_active)
    products = products.order_by('category__position', 'position')

    selected_product = request.GET.get('product')
    if selected_product:
        try:
            selected_product = int(selected_product)
        except:
            selected_product = None

    _c = None
    for p in products:
        if p.category.name != _c:
            p.printed_category = p.category.name
            _c = p.category.name

    form = OrderForm()
    if request.method == 'POST':
        data = request.POST
        form = OrderForm(data)
        if form.is_valid():
            name = form.cleaned_data.get('name')
            phone = form.cleaned_data.get('phone')
            address = form.cleaned_data.get('address')
            comment = form.cleaned_data.get('comment')

            email_body = u'''
Данные пользователя
-------------------------------
Имя: {name}
Телефон: {phone}
Адрес: {address}
Комментарий: {comment}
-------------------------------
            '''.format(
                name=name,
                phone=phone,
                address=address,
                comment=comment,
            )

            status = OrderStatus.objects.get(default=True)
            order = Order(
                name=name,
                phone=phone,
                address=address,
                comment=comment,
                price=0,
                purchase_price=0,
                status=status,
            )
            order.save()

            order_price = 0
            order_purchase_price = 0
            for product in products:
                _amount = request.POST.get('product-amount-%d' % product.id)
                if _amount:
                    try:
                        amount = int(_amount)
                        if amount > 0:
                            orderitem = OrderItem(
                                order=order,
                                product=product,
                                amount=amount,
                                purchase_price_for_one=product.purchase_price,
                                purchase_price=product.purchase_price*amount,
                                price_for_one=product.price,
                                price=product.price*amount,
                            )
                            orderitem.save()
                            order_price += product.price * amount
                            order_purchase_price += product.purchase_price * amount
                            email_body += u'''
{product:<30}  |  {price_for_one} грн.  |  {amount}  |  {price} грн.
                            '''.format(
                                product=product.name,
                                amount=amount,
                                price_for_one=product.price,
                                price=product.price*amount,
                            )
                    except:
                        pass
            order.price = order_price
            order.purchase_price = order_purchase_price
            order.save()
            email_body += u'''
-------------------------------
Всего: {price} грн.
В закупке: {purchase_price} грн.
            '''.format(
                price=order_price,
                purchase_price=order_purchase_price,
            )

            # Send email to managers here
            send_mail(
                u'Новый заказ на Cityfood',
                email_body,
                settings.DEFAULT_FROM_EMAIL,
                settings.MANAGER_EMAILS,
                fail_silently=False,
            )

            return redirect(reverse('order_success')+'?order=%d' % order.id)

    return render_to_response(
        'order/order.html', RequestContext(request, {
            'products': products,
            'form': form,
            'selected_product': selected_product,
        }))


def order_success(request):
    order_id = request.GET.get('order')

    if order_id:
        try:
            order = Order.objects.get(id=order_id)
        except Order.DoesNotExist:
            raise Http404
    else:
        raise Http404

    return render_to_response(
        'order/order_success.html', RequestContext(request, {
            'order': order,
        }))
