# coding: utf-8
from django.contrib import admin
from django.shortcuts import render
from .models import Order, OrderItem, OrderStatus
from suit.admin import SortableModelAdmin


class OrderItemInline(admin.StackedInline):
    model = OrderItem
    extra = 0


class OrderAdmin(admin.ModelAdmin):
    list_display = ['added', 'name', 'phone', 'address', 'price', 'status']
    list_editable = ['status']
    inlines = [OrderItemInline]
    search_fields = ['name', 'phone', 'address']
    date_hierarchy = 'added'
    actions = ['print_order']

    def print_order(self, request, querysets):
        orders = querysets.order_by('added')

        products_for_purchase = []
        for o in orders:
            for i in o.orderitem_set.all():
                if i.product.linked_products.count() > 0:
                    for t in i.product.linked_products.all():
                        products_for_purchase.append(OrderItem(
                            order=o,
                            product=t,
                            purchase_price_for_one=t.purchase_price,
                            purchase_price=t.purchase_price*i.amount,
                            price_for_one=t.price,
                            amount=i.amount,
                            price=t.price*i.amount))
                else:
                    products_for_purchase.append(i)
        products_for_purchase = sorted(
            products_for_purchase,
            key=lambda item: item.product.name,
        )

        p_ids = set(map(lambda x: x.product.id, products_for_purchase))
        product_groups = [[y for y in products_for_purchase if y.product.id == x] for x in p_ids]

        new_product_list = []
        for g in product_groups:
            new_item = OrderItem(
                order_id=1,
                product_id=1,
                purchase_price_for_one=0,
                purchase_price=0,
                price_for_one=0,
                amount=0,
                price=0)
            for o in g:
                new_item.order = o.order
                new_item.product = o.product
                new_item.purchase_price_for_one = o.purchase_price_for_one
                new_item.purchase_price += o.purchase_price
                new_item.price_for_one = o.price_for_one
                new_item.price += o.price
                new_item.amount += o.amount
            new_product_list.append(new_item)
        products_for_purchase = new_product_list

        purchase_price = 0
        total_price = 0
        for p in products_for_purchase:
            purchase_price += p.purchase_price
            total_price += p.price

        boxing = {}
        for p in products_for_purchase:
            if p.product.boxing.name in boxing:
                boxing[p.product.boxing.name] += p.amount
            else:
                boxing[p.product.boxing.name] = p.amount

        return render(request, 'order/print_order.html', {
            'orders': orders,
            'order_statuses': OrderStatus.objects.exclude(default=True),
            'products_for_purchase': products_for_purchase,
            'purchase_price': purchase_price,
            'total_price': total_price,
            'boxing': boxing.items(),
        })
    print_order.short_description = u'Печать заказов'


class OrderStatusAdmin(SortableModelAdmin):
    list_display = ['name', 'default']
    list_editable = ['default']
    search_fields = ['name']
    sortable = 'position'

admin.site.register(Order, OrderAdmin)
admin.site.register(OrderStatus, OrderStatusAdmin)
