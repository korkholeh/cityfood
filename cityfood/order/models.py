# coding: utf-8
from django.db import models


class OrderStatus(models.Model):
    name = models.CharField(u'Название', max_length=100)
    position = models.IntegerField(u'Позиция', default=1)
    default = models.BooleanField(u'По умолчанию', default=False)

    class Meta:
        ordering = ['position']
        verbose_name = u'Статус заказа'
        verbose_name_plural = u'Статусы заказов'

    def __unicode__(self):
        return self.name


class Order(models.Model):
    name = models.CharField(u'Имя', max_length=200)
    phone = models.CharField(u'Телефон', max_length=50)
    address = models.TextField(u'Адрес доставки')
    comment = models.TextField(u'Комментарий', blank=True)

    purchase_price = models.DecimalField(
        u'Цена в закупке', max_digits=5, decimal_places=2)
    price = models.DecimalField(u'Цена', max_digits=5, decimal_places=2)
    status = models.ForeignKey(OrderStatus, verbose_name=u'Статус')

    added = models.DateTimeField(u'Добавлен', auto_now_add=True)
    modified = models.DateTimeField(u'Изменен', auto_now=True)

    class Meta:
        ordering = ['-added']
        verbose_name = u'Заказ'
        verbose_name_plural = u'Заказы'


class OrderItem(models.Model):
    order = models.ForeignKey(Order, verbose_name=u'Заказ')
    product = models.ForeignKey('catalog.Product', verbose_name=u'Товар')
    amount = models.IntegerField(u'Количество')

    purchase_price_for_one = models.DecimalField(
        u'Цена за единицу в закупке',
        max_digits=5, decimal_places=2)
    purchase_price = models.DecimalField(
        u'Цена за позицию в закупке',
        max_digits=5, decimal_places=2)

    price_for_one = models.DecimalField(
        u'Цена за единицу',
        max_digits=5, decimal_places=2)
    price = models.DecimalField(
        u'Цена за позицию',
        max_digits=5, decimal_places=2)

    class Meta:
        verbose_name = u'Элемент заказа'
        verbose_name_plural = u'Элементы заказа'

    def __unicode__(self):
        return unicode(self.product)
