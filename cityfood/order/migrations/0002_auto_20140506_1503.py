# encoding: utf8
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        (b'order', b'0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name=b'order',
            name=b'comment',
            field=models.TextField(blank=True),
        ),
    ]
