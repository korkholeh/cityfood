# encoding: utf8
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        (b'catalog', b'__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name=b'Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                (b'name', models.CharField(max_length=200)),
                (b'phone', models.CharField(max_length=50)),
                (b'address', models.TextField()),
                (b'comment', models.TextField()),
                (b'price', models.DecimalField(max_digits=5, decimal_places=2)),
                (b'added', models.DateTimeField(auto_now_add=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name=b'OrderItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                (b'order', models.ForeignKey(to=b'order.Order', to_field='id')),
                (b'product', models.ForeignKey(to=b'catalog.Product', to_field='id')),
                (b'amount', models.IntegerField()),
                (b'price', models.DecimalField(max_digits=5, decimal_places=2)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
