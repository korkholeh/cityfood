# encoding: utf8
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        (b'order', b'0004_auto_20140512_1112'),
    ]

    operations = [
        migrations.AddField(
            model_name=b'orderitem',
            name=b'purchase_price_for_one',
            field=models.DecimalField(default=0, verbose_name='\u0426\u0435\u043d\u0430 \u0437\u0430 \u0435\u0434\u0438\u043d\u0438\u0446\u0443 \u0432 \u0437\u0430\u043a\u0443\u043f\u043a\u0435', max_digits=5, decimal_places=2),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name=b'orderitem',
            name=b'purchase_price',
            field=models.DecimalField(default=0, verbose_name='\u0426\u0435\u043d\u0430 \u0437\u0430 \u043f\u043e\u0437\u0438\u0446\u0438\u044e \u0432 \u0437\u0430\u043a\u0443\u043f\u043a\u0435', max_digits=5, decimal_places=2),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name=b'order',
            name=b'modified',
            field=models.DateTimeField(default=datetime.datetime(2014, 5, 21, 12, 14, 40, 191901), verbose_name='\u0418\u0437\u043c\u0435\u043d\u0435\u043d', auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name=b'order',
            name=b'purchase_price',
            field=models.DecimalField(default=0, verbose_name='\u0426\u0435\u043d\u0430 \u0432 \u0437\u0430\u043a\u0443\u043f\u043a\u0435', max_digits=5, decimal_places=2),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name=b'orderitem',
            name=b'price_for_one',
            field=models.DecimalField(default=0, verbose_name='\u0426\u0435\u043d\u0430 \u0437\u0430 \u0435\u0434\u0438\u043d\u0438\u0446\u0443', max_digits=5, decimal_places=2),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name=b'orderitem',
            name=b'price',
            field=models.DecimalField(verbose_name='\u0426\u0435\u043d\u0430 \u0437\u0430 \u043f\u043e\u0437\u0438\u0446\u0438\u044e', max_digits=5, decimal_places=2),
        ),
        migrations.AlterField(
            model_name=b'order',
            name=b'name',
            field=models.CharField(max_length=200, verbose_name='\u0418\u043c\u044f'),
        ),
        migrations.AlterField(
            model_name=b'order',
            name=b'address',
            field=models.TextField(verbose_name='\u0410\u0434\u0440\u0435\u0441 \u0434\u043e\u0441\u0442\u0430\u0432\u043a\u0438'),
        ),
    ]
