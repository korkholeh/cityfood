# encoding: utf8
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        (b'order', b'0003_auto_20140506_1719'),
    ]

    operations = [
        migrations.AlterField(
            model_name=b'orderstatus',
            name=b'position',
            field=models.IntegerField(default=1, verbose_name='\u041f\u043e\u0437\u0438\u0446\u0438\u044f'),
        ),
        migrations.AlterField(
            model_name=b'orderitem',
            name=b'order',
            field=models.ForeignKey(to=b'order.Order', to_field='id', verbose_name='\u0417\u0430\u043a\u0430\u0437'),
        ),
        migrations.AlterField(
            model_name=b'orderitem',
            name=b'amount',
            field=models.IntegerField(verbose_name='\u041a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e'),
        ),
        migrations.AlterField(
            model_name=b'order',
            name=b'address',
            field=models.TextField(verbose_name='\u0410\u0434\u0440\u0435\u0441'),
        ),
        migrations.AlterField(
            model_name=b'orderitem',
            name=b'price',
            field=models.DecimalField(verbose_name='\u0426\u0435\u043d\u0430', max_digits=5, decimal_places=2),
        ),
        migrations.AlterField(
            model_name=b'order',
            name=b'name',
            field=models.CharField(max_length=200, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435'),
        ),
        migrations.AlterField(
            model_name=b'order',
            name=b'comment',
            field=models.TextField(verbose_name='\u041a\u043e\u043c\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0439', blank=True),
        ),
        migrations.AlterField(
            model_name=b'order',
            name=b'price',
            field=models.DecimalField(verbose_name='\u0426\u0435\u043d\u0430', max_digits=5, decimal_places=2),
        ),
        migrations.AlterField(
            model_name=b'orderitem',
            name=b'product',
            field=models.ForeignKey(to=b'catalog.Product', to_field='id', verbose_name='\u0422\u043e\u0432\u0430\u0440'),
        ),
        migrations.AlterField(
            model_name=b'orderstatus',
            name=b'name',
            field=models.CharField(max_length=100, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435'),
        ),
        migrations.AlterField(
            model_name=b'order',
            name=b'status',
            field=models.ForeignKey(to=b'order.OrderStatus', to_field='id', verbose_name='\u0421\u0442\u0430\u0442\u0443\u0441'),
        ),
        migrations.AlterField(
            model_name=b'order',
            name=b'added',
            field=models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u043e\u0431\u0430\u0432\u043b\u0435\u043d'),
        ),
        migrations.AlterField(
            model_name=b'order',
            name=b'phone',
            field=models.CharField(max_length=50, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d'),
        ),
        migrations.AlterField(
            model_name=b'orderstatus',
            name=b'default',
            field=models.BooleanField(default=False, verbose_name='\u041f\u043e \u0443\u043c\u043e\u043b\u0447\u0430\u043d\u0438\u044e'),
        ),
    ]
