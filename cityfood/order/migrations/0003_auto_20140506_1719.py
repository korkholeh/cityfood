# encoding: utf8
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        (b'order', b'0002_auto_20140506_1503'),
    ]

    operations = [
        migrations.CreateModel(
            name=b'OrderStatus',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                (b'name', models.CharField(max_length=100)),
                (b'position', models.IntegerField(default=1)),
                (b'default', models.BooleanField(default=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name=b'order',
            name=b'status',
            field=models.ForeignKey(to=b'order.OrderStatus', default=1, to_field='id'),
            preserve_default=False,
        ),
    ]
