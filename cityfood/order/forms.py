# coding: utf-8
from django import forms


class OrderForm(forms.Form):
    name = forms.CharField(label=u'Имя')
    phone = forms.CharField(label=u'Телефон')
    address = forms.CharField(label=u'Адрес доставки')
    comment = forms.CharField(
        label=u'Комментарий',
        required=False, widget=forms.Textarea)
