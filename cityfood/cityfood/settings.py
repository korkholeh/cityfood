# coding: utf-8
"""
Django settings for cityfood project.

For more information on this file, see
https://docs.djangoproject.com/en/dev/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/dev/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/dev/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'a!%t18lsp(ejq@doqp9r@dgd!f4s8db6prwa$@)=0qxqckud3='

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'suit',
    'django.contrib.admin',
    'suit_ckeditor',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django.contrib.flatpages',
    'crispy_forms',
    'robots',
    'pages',
    'catalog',
    'order',
    'navigation',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
)

ROOT_URLCONF = 'cityfood.urls'

WSGI_APPLICATION = 'cityfood.wsgi.application'

from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS as TCP
TEMPLATE_CONTEXT_PROCESSORS = TCP + (
    'django.core.context_processors.request',
)

# Database
# https://docs.djangoproject.com/en/dev/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/dev/topics/i18n/

LANGUAGE_CODE = 'ru'

TIME_ZONE = 'Europe/Kiev'

USE_I18N = True

USE_L10N = True

USE_TZ = True

SITE_ID = 1

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/dev/howto/static-files/

STATIC_URL = '/static/'

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)

PUBLIC_DIR = os.path.join(BASE_DIR, '..', 'public')
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(PUBLIC_DIR, 'media')
STATIC_ROOT = os.path.join(PUBLIC_DIR, 'static')

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'templates'),
)

SUIT_CONFIG = {
    'ADMIN_NAME': u'CityFood',
    'HEADER_DATE_FORMAT': 'l, j F Y',   # Saturday, 16 March 2013
    'HEADER_TIME_FORMAT': 'H:i',        # 18:42
    # FIX
    # 'SEARCH_URL': 'admin:accounts_account_changelist',

    'MENU': (
        {
            'label': u'Учетные записи',
            'icon': 'icon-user',
            'models': (
                'auth.user',
                'auth.group',
            )
        },
        {
            'label': u'Страницы',
            'icon': 'icon-file',
            'models': (
                'navigation.navigationitem',
                'flatpages.flatpage',
            )
        },
        {
            'label': u'Номенклатура',
            'icon': 'icon-align-justify',
            'models': (
                'catalog.product',
                'catalog.category',
                'catalog.boxing',
            )
        },
        {
            'label': u'Заказы',
            'icon': 'icon-shopping-cart',
            'models': (
                'order.order',
                'order.orderstatus',
            )
        },
        {
            'label': u'Настройки',
            'icon': 'icon-cog',
            'models': (
                'sites.site',
                'robots.url',
                'robots.rule',
            )
        },
    )}


CRISPY_TEMPLATE_PACK = 'bootstrap3'

ROBOTS_USE_SITEMAP = False

EMAIL_DEBUG = DEBUG
CONTACT_EMAIL = 'mailbox@cityfood.com.ua'
EMAIL_HOST = 'localhost'
EMAIL_PORT = 1025
DEFAULT_FROM_EMAIL = 'mailbox@cityfood.com.ua'

MANAGER_EMAILS = [
    'manager@cityfood.com.ua',
]

ALERT_BOXING_STOCK = 10

from local_settings import *
