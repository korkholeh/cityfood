from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.contrib.sitemaps import FlatPageSitemap


sitemaps = {
    'flatpages': FlatPageSitemap,
}

urlpatterns = patterns(
    '',
    url(r'^$', 'cityfood.views.home', name='home'),
    url(r'^order/', include('order.urls')),
    url(r'^pages/', include('django.contrib.flatpages.urls')),
    url(r'^admin/', include(admin.site.urls)),
    (r'^robots\.txt$', include('robots.urls')),
    url(
        r'^sitemap\.xml$',
        'django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps}),
)

if getattr(settings, 'DEBUG', False):
    urlpatterns += patterns(
        'django.contrib.staticfiles.views',
        url(r'^static/(?P<path>.*)$', 'serve'),
    )
    urlpatterns += patterns(
        '',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': getattr(settings, 'MEDIA_ROOT', ''),
            'show_indexes': True,
            }),
    )
