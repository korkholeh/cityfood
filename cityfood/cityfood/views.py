from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext


def home(request):
    from catalog.models import Product
    products = Product.objects.filter(
        active=True, promo=True)

    non_active = Product.objects.filter(active=False)
    products = products.exclude(linked_products__in=non_active)

    return render_to_response(
        'home.html', RequestContext(request, {
            'products': products,
        }))
