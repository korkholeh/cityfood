# coding: utf-8
from django.db import models
from django.contrib.flatpages.models import FlatPage


class MetaForPage(models.Model):
    page = models.OneToOneField(FlatPage, related_name='meta_data')
    meta_description = models.CharField(u'META description', max_length=300)
    meta_keywords = models.CharField(u'META keywords', max_length=300)
