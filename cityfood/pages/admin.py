# coding: utf-8
from django.contrib import admin
from django.contrib.flatpages.models import FlatPage
from django import forms
from .models import MetaForPage

import json
from django.utils.safestring import mark_safe
from suit_ckeditor.widgets import CKEditorWidget

class MyCKEditorWidget(CKEditorWidget):
    def render(self, name, value, attrs=None):
        output = super(CKEditorWidget, self).render(name, value, attrs)
        output += mark_safe(
            '<script type="text/javascript">CKEDITOR.replace("%s", %s);</script>'
            % (self.attrs['id'], json.dumps(self.editor_options)))
        return output


class PageForm(forms.ModelForm):
    class Meta:
        model = FlatPage
        widgets = {
            'content': MyCKEditorWidget(
                attrs={'id': 'id_content'},
                editor_options={'startupFocus': True})
        }


class MetaData(admin.StackedInline):
    model = MetaForPage


class FlatPageCustom(admin.ModelAdmin):
    form = PageForm
    inlines = [MetaData]


admin.site.unregister(FlatPage)
admin.site.register(FlatPage, FlatPageCustom)
