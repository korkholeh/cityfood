# encoding: utf8
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        (b'flatpages', b'__first__'),
        (b'sites', b'__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name=b'MetaForPage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                (b'page', models.OneToOneField(to=b'flatpages.FlatPage', to_field='id')),
                (b'meta_description', models.CharField(max_length=300, verbose_name='META description')),
                (b'meta_keywords', models.CharField(max_length=300, verbose_name='META description')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
