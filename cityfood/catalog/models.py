# coding: utf-8
from django.db import models


class Boxing(models.Model):
    name = models.CharField(u'Название', max_length=200)
    image = models.ImageField(u'Изображение', upload_to='boxing-images')
    active = models.BooleanField(u'Активный', default=True)
    stock = models.IntegerField(u'Запас', default=0)
    purchase_price = models.DecimalField(
        u'Цена закупки', max_digits=5, decimal_places=2)

    added = models.DateTimeField(u'Добавлено', auto_now_add=True)
    modified = models.DateTimeField(u'Обновлено', auto_now=True)

    class Meta:
        ordering = ['name']
        verbose_name = u'Упаковка'
        verbose_name_plural = u'Упаковки'

    def __unicode__(self):
        return self.name


class Category(models.Model):
    name = models.CharField(u'Название', max_length=200)
    active = models.BooleanField(u'Активный', default=True)
    position = models.IntegerField(u'Позиция', default=1)

    added = models.DateTimeField(u'Добавлено', auto_now_add=True)
    modified = models.DateTimeField(u'Обновлено', auto_now=True)

    class Meta:
        ordering = ['position']
        verbose_name = u'Категория'
        verbose_name_plural = u'Категории'

    def __unicode__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(u'Название', max_length=200)
    category = models.ForeignKey(Category, verbose_name=u'Категория')
    boxing = models.ForeignKey(
        Boxing, verbose_name=u'Упаковка', null=True, blank=True)
    image = models.ImageField(u'Изображение', upload_to='product-images')
    description = models.TextField(u'Описание', blank=True)
    price = models.DecimalField(u'Цена', max_digits=5, decimal_places=2)

    purchase_price = models.DecimalField(
        u'Цена закупки', max_digits=5, decimal_places=2)

    linked_products = models.ManyToManyField(
        'self', blank=True, related_name='complex',
        symmetrical=False,
        help_text=u'Только для наборов. Укажите элементы набора.')

    memo = models.TextField(u'Заметки', blank=True)
    active = models.BooleanField(u'Активный', default=True)
    promo = models.BooleanField(
        u'Продвигаемый', default=True,
        help_text=u'Видимый в карусели на главной странице.')
    position = models.IntegerField(u'Позиция', default=1)

    added = models.DateTimeField(u'Добавлено', auto_now_add=True)
    modified = models.DateTimeField(u'Обновлено', auto_now=True)

    class Meta:
        ordering = ['position']
        verbose_name = u'Товар'
        verbose_name_plural = u'Товары'

    def __unicode__(self):
        return self.name

    def get_boxing_price(self):
        if self.linked_products.count() == 0:
            if self.boxing is None:
                return 0
            else:
                return self.boxing.purchase_price
        else:
            bp = 0
            for i in self.linked_products.all():
                if i.boxing is not None:
                    bp += i.boxing.purchase_price
            return bp

    def get_boxing_name(self):
        if self.linked_products.count() == 0:
            if self.boxing is None:
                return ''
            else:
                return self.boxing.name
        else:
            bn = []
            for i in self.linked_products.all():
                if i.boxing is not None:
                    bn.append(i.boxing.name)
            return ','.join(bn)

    def get_purchase_and_boxing_price(self):
        return self.purchase_price + self.get_boxing_price()

    def get_delta(self):
        return self.price - self.get_purchase_and_boxing_price()
