# encoding: utf8
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        (b'catalog', b'0003_auto_20140512_1108'),
    ]

    operations = [
        migrations.CreateModel(
            name=b'Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                (b'name', models.CharField(max_length=200, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                (b'active', models.BooleanField(default=True, verbose_name='\u0410\u043a\u0442\u0438\u0432\u043d\u044b\u0439')),
                (b'position', models.IntegerField(default=1, verbose_name='\u041f\u043e\u0437\u0438\u0446\u0438\u044f')),
                (b'added', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u043e\u0431\u0430\u0432\u043b\u0435\u043d\u0430')),
                (b'modified', models.DateTimeField(auto_now=True, verbose_name='\u0418\u0441\u043f\u0440\u0430\u0432\u043b\u0435\u043d\u0430')),
            ],
            options={
                'ordering': [b'position'],
                'verbose_name': '\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f',
                'verbose_name_plural': '\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name=b'Boxing',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                (b'name', models.CharField(max_length=200, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                (b'active', models.BooleanField(default=True, verbose_name='\u0410\u043a\u0442\u0438\u0432\u043d\u044b\u0439')),
                (b'stock', models.IntegerField(default=0, verbose_name='\u0417\u0430\u043f\u0430\u0441')),
                (b'added', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u043e\u0431\u0430\u0432\u043b\u0435\u043d\u0430')),
                (b'modified', models.DateTimeField(auto_now=True, verbose_name='\u0418\u0441\u043f\u0440\u0430\u0432\u043b\u0435\u043d\u0430')),
            ],
            options={
                'ordering': [b'name'],
                'verbose_name': '\u0423\u043f\u0430\u043a\u043e\u0432\u043a\u0430',
                'verbose_name_plural': '\u0423\u043f\u0430\u043a\u043e\u0432\u043a\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name=b'product',
            name=b'linked_products',
            field=models.ManyToManyField(default=None, to=b'catalog.Product', blank=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name=b'product',
            name=b'added',
            field=models.DateTimeField(default=datetime.datetime(2014, 5, 21, 9, 38, 59, 344323), verbose_name='\u0414\u043e\u0431\u0430\u0432\u043b\u0435\u043d', auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name=b'product',
            name=b'purchase_price',
            field=models.DecimalField(default=0, verbose_name='\u0426\u0435\u043d\u0430 \u0437\u0430\u043a\u0443\u043f\u043a\u0438', max_digits=5, decimal_places=2),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name=b'product',
            name=b'boxing',
            field=models.ForeignKey(default=1, verbose_name='\u0423\u043f\u0430\u043a\u043e\u0432\u043a\u0430', to_field='id', to=b'catalog.Boxing'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name=b'product',
            name=b'memo',
            field=models.TextField(default=b'', verbose_name='\u0417\u0430\u043c\u0435\u0442\u043a\u0438', blank=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name=b'product',
            name=b'modified',
            field=models.DateTimeField(default=datetime.datetime(2014, 5, 21, 9, 39, 53, 194155), verbose_name='\u0418\u0441\u043f\u0440\u0430\u0432\u043b\u0435\u043d', auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name=b'product',
            name=b'category',
            field=models.ForeignKey(default=1, verbose_name='\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f', to_field='id', to=b'catalog.Category'),
            preserve_default=False,
        ),
    ]
