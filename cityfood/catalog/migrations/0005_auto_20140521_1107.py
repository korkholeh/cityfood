# encoding: utf8
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        (b'catalog', b'0004_auto_20140521_0939'),
    ]

    operations = [
        migrations.AlterField(
            model_name=b'product',
            name=b'boxing',
            field=models.ForeignKey(verbose_name='\u0423\u043f\u0430\u043a\u043e\u0432\u043a\u0430', to_field='id', blank=True, to=b'catalog.Boxing', null=True),
        ),
    ]
