# encoding: utf8
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        (b'catalog', b'0007_auto_20140521_1120'),
    ]

    operations = [
        migrations.AddField(
            model_name=b'boxing',
            name=b'purchase_price',
            field=models.DecimalField(default=0, verbose_name='\u0426\u0435\u043d\u0430 \u0437\u0430\u043a\u0443\u043f\u043a\u0438', max_digits=5, decimal_places=2),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name=b'product',
            name=b'promo',
            field=models.BooleanField(default=True, help_text='\u0412\u0438\u0434\u0438\u043c\u044b\u0439 \u0432 \u043a\u0430\u0440\u0443\u0441\u0435\u043b\u0438 \u043d\u0430 \u0433\u043b\u0430\u0432\u043d\u043e\u0439 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0435.', verbose_name='\u041f\u0440\u043e\u0434\u0432\u0438\u0433\u0430\u0435\u043c\u044b\u0439'),
        ),
    ]
