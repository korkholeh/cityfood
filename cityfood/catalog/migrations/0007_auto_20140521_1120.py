# encoding: utf8
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        (b'catalog', b'0006_boxing_image'),
    ]

    operations = [
        migrations.AddField(
            model_name=b'product',
            name=b'promo',
            field=models.BooleanField(default=True, verbose_name='\u041f\u0440\u043e\u0434\u0432\u0438\u0433\u0430\u0435\u043c\u044b\u0439'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name=b'category',
            name=b'modified',
            field=models.DateTimeField(auto_now=True, verbose_name='\u041e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u043e'),
        ),
        migrations.AlterField(
            model_name=b'category',
            name=b'added',
            field=models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u043e\u0431\u0430\u0432\u043b\u0435\u043d\u043e'),
        ),
        migrations.AlterField(
            model_name=b'boxing',
            name=b'added',
            field=models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u043e\u0431\u0430\u0432\u043b\u0435\u043d\u043e'),
        ),
        migrations.AlterField(
            model_name=b'product',
            name=b'added',
            field=models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u043e\u0431\u0430\u0432\u043b\u0435\u043d\u043e'),
        ),
        migrations.AlterField(
            model_name=b'product',
            name=b'modified',
            field=models.DateTimeField(auto_now=True, verbose_name='\u041e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u043e'),
        ),
        migrations.AlterField(
            model_name=b'boxing',
            name=b'modified',
            field=models.DateTimeField(auto_now=True, verbose_name='\u041e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u043e'),
        ),
    ]
