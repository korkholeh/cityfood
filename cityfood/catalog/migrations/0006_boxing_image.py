# encoding: utf8
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        (b'catalog', b'0005_auto_20140521_1107'),
    ]

    operations = [
        migrations.AddField(
            model_name=b'boxing',
            name=b'image',
            field=models.ImageField(default=None, upload_to=b'boxing-images', verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435'),
            preserve_default=False,
        ),
    ]
