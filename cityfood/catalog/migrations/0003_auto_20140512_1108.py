# encoding: utf8
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        (b'catalog', b'0002_auto_20140506_1344'),
    ]

    operations = [
        migrations.AddField(
            model_name=b'product',
            name=b'position',
            field=models.IntegerField(default=1, verbose_name='\u041f\u043e\u0437\u0438\u0446\u0438\u044f'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name=b'product',
            name=b'name',
            field=models.CharField(max_length=200, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435'),
        ),
        migrations.AlterField(
            model_name=b'product',
            name=b'price',
            field=models.DecimalField(verbose_name='\u0426\u0435\u043d\u0430', max_digits=5, decimal_places=2),
        ),
        migrations.AlterField(
            model_name=b'product',
            name=b'image',
            field=models.ImageField(upload_to=b'product-images', verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435'),
        ),
        migrations.AlterField(
            model_name=b'product',
            name=b'active',
            field=models.BooleanField(default=True, verbose_name='\u0410\u043a\u0442\u0438\u0432\u043d\u044b\u0439'),
        ),
        migrations.AlterField(
            model_name=b'product',
            name=b'description',
            field=models.TextField(verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True),
        ),
    ]
