# coding: utf-8
from django.contrib import admin
from django.conf.urls import patterns, url
from django.core.urlresolvers import reverse
from django.shortcuts import render
from .models import (
    Boxing,
    Category,
    Product,
)
from suit.admin import SortableModelAdmin
from suit.widgets import NumberInput
from django import forms
from django.conf import settings


class EditBoxingForm(forms.ModelForm):
    class Meta:
        model = Boxing
        widgets = {
            'stock': NumberInput(attrs={'class': 'input-mini'}),
            'purchase_price': forms.NumberInput(attrs={'class': 'input-mini'})
        }


class BoxingAdmin(admin.ModelAdmin):
    list_display = [
        'image_preview', 'name', 'active',
        'stock', 'purchase_price', 'modified']
    list_editable = ['active', 'stock', 'purchase_price']
    date_hierarchy = 'modified'
    search_fields = ['name']
    list_display_links = ['image_preview', 'name']
    form = EditBoxingForm

    def get_changelist_form(self, request, **kwargs):
        kwargs.setdefault('form', EditBoxingForm)
        return super(BoxingAdmin, self).get_changelist_form(request, **kwargs)

    def suit_row_attributes(self, obj, request):
        if obj.stock <= settings.ALERT_BOXING_STOCK:
            return {'class': 'error'}

    def image_preview(self, obj):
        if obj.image:
            return u'<img src="{0}" style="width:100px;"/>'.format(
                obj.image.url)
        else:
            return u'Без картинки'
    image_preview.allow_tags = True
    image_preview.short_description = u'Изображение'


admin.site.register(Boxing, BoxingAdmin)


class CategoryAdmin(SortableModelAdmin):
    list_display = ['name', 'active', 'modified']
    list_editable = ['active']
    date_hierarchy = 'modified'
    search_fields = ['name']
    sortable = 'position'

admin.site.register(Category, CategoryAdmin)


class EditProductForm(forms.ModelForm):
    class Meta:
        model = Product
        widgets = {
            'price': forms.NumberInput(attrs={'class': 'input-mini'}),
            'purchase_price': forms.NumberInput(attrs={'class': 'input-mini'})
        }


class ProductAdmin(SortableModelAdmin):
    list_display = [
        'image_preview',
        'name', 'active', 'price', 'purchase_price',
        'category', 'boxing',
        'memo']
    list_editable = ['active', 'price', 'purchase_price']
    list_display_links = ['image_preview', 'name']
    sortable = 'position'
    search_fields = ['name']
    list_filter = ['category', 'boxing']
    filter_horizontal = ['linked_products']
    form = EditProductForm
    fieldsets = (
        (None, {
            'fields': (
                'name', 'active', 'description',
                'image',
                'category', 'boxing', 'promo')
        }),
        (u'Цена', {
            'fields': ('price', 'purchase_price',)
        }),
        (u'Комплексные наборы', {
            'fields': ('linked_products',)
        }),
        (u'Дополнительно', {
            'fields': ('memo',)
        }),
    )

    def get_changelist_form(self, request, **kwargs):
        kwargs.setdefault('form', EditProductForm)
        return super(ProductAdmin, self).get_changelist_form(request, **kwargs)

    def image_preview(self, obj):
        if obj.image:
            return u'<img src="{0}" style="width:100px;"/>'.format(
                obj.image.url)
        else:
            return u'Без картинки'
    image_preview.allow_tags = True
    image_preview.short_description = u'Изображение'

    def get_urls(self):
        urls = super(ProductAdmin, self).get_urls()
        admin_site = self.admin_site
        opts = self.model._meta
        info = opts.app_label, opts.module_name,
        session_urls = patterns(
            "",
            url(
                "^check-menu/$",
                admin_site.admin_view(self.check_menu_view),
                name='%s_%s_checkmenu' % info),
            url(
                "^print-menu/$",
                admin_site.admin_view(self.print_menu_view),
                name='%s_%s_printmenu' % info),
            url(
                "^all-products/$",
                admin_site.admin_view(self.all_products_view),
                name='%s_%s_allproducts' % info),
        )
        return session_urls + urls

    def all_products_view(self, request, extra_context=None):
        from django.core.exceptions import PermissionDenied
        import datetime
        # check if user has change or add permissions for model
        if not self.has_change_permission(request) \
                and not self.has_add_permission(request):
            raise PermissionDenied
        # Print menu
        products = Product.objects.all()
        return render(request, 'catalog/all-products.html', {
            'products': products,
            'date': datetime.date.today(),
        })

    def check_menu_view(self, request, extra_context=None):
        from django.core.exceptions import PermissionDenied
        import datetime
        # check if user has change or add permissions for model
        if not self.has_change_permission(request) \
                and not self.has_add_permission(request):
            raise PermissionDenied
        # Print menu
        products = Product.objects.filter(linked_products=None)
        return render(request, 'catalog/check-menu.html', {
            'products': products,
            'date': datetime.date.today(),
        })

    def print_menu_view(self, request, extra_context=None):
        from django.core.exceptions import PermissionDenied
        import datetime
        # check if user has change or add permissions for model
        if not self.has_change_permission(request) \
                and not self.has_add_permission(request):
            raise PermissionDenied
        # Print menu
        products = Product.objects.filter(active=True)
        return render(request, 'catalog/menu.html', {
            'products': products,
            'date': datetime.date.today(),
        })

    def changelist_view(self, request, extra_context=None):
        """Renders the change view."""
        opts = self.model._meta
        context = {
            "checkmenu_url": reverse(
                "%s:%s_%s_checkmenu" % (
                    self.admin_site.name,
                    opts.app_label,
                    opts.module_name)),
            "printmenu_url": reverse(
                "%s:%s_%s_printmenu" % (
                    self.admin_site.name,
                    opts.app_label,
                    opts.module_name)),
            "allproducts_url": reverse(
                "%s:%s_%s_allproducts" % (
                    self.admin_site.name,
                    opts.app_label,
                    opts.module_name)),
        }
        context.update(extra_context or {})
        return super(ProductAdmin, self).changelist_view(request, context)


admin.site.register(Product, ProductAdmin)
